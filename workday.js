const request = require("request-promise")
const cheerio = require("cheerio")
const writeXlsxFile = require('write-excel-file/node')
const puppeteer = require('puppeteer');
const fs = require("fs")


const $ = cheerio.load(fs.readFileSync('workday.html'))

// titles 
const scrapeWorkday = async () => {
    const titles = []
    // const titlesText = $("div[class='WE0Y WMYY WBAB WF1Y WD1Y WO5F WK5F']").text()
    $("div[class='WE0Y WMYY WBAB WF1Y WD1Y WO5F WK5F']").find('div > ul > li > div > div > div > div').each(function (index, element) {
        titles.push($(element).text());
    });
    // console.dir(titles);

    // heads 
    const heads = []
    $("span[class='gwt-InlineLabel WLAG WK5F']").each(function (index, element) {
        heads.push($(element).text());
    });
    // console.dir(heads);

    // dates and descriptions 
    const dates = []
    const descriptions = []
    const deliveryModes = []
    const courseTags = []
    const eligibilities = []
    $("div[class='WKYF']").find('label').each(function (index, element) {
        if ($(element).text() == "Start/End Date") {
            dates.push($(element).next().text());
        } else if ($(element).text() == "Description") {
            descriptions.push($(element).next().text())
        } else if ($(element).text() == "Delivery Mode") {
            deliveryModes.push($(element).next().find("div > ul > li > div > div > div > div").text())
        } 
        // else if ($(element).text() == "Course Tags") {
        //     courseTags.push($(element).next().find("div > ul > li > div > div > div > div").text())
        // } else if ($(element).text() == "Student Eligibility Rule") {
        //     eligibilities.push($(element).next().find("div").text())
        // } 
    });
    // console.dir(dates);
    console.dir(eligibilities)
    const data = [
    ]


    let headers = []
    let title = {
        "value" : "Title",
        fontWeight: 'bold'
    }
    headers.push(title)
    let head = {
        "value" : "Headers",
        fontWeight: 'bold'
    }
    headers.push(head)
    let startDates = {
        "value" : "Start Date",
        fontWeight: 'bold'
    }
    headers.push(startDates)
    let description = {
        "value" : "Description",
        fontWeight: 'bold'
    }
    headers.push(description)
    let deliveryMode = {
        "value" : "Delivery Mode",
        fontWeight: 'bold'
    }
    headers.push(deliveryMode)
    data.push(headers)
    for (var j=0;j<titles.length;j++) {
        let columns = []
        columns.push ({
            value: titles[j]
        })
        columns.push({
            value: heads[j]
        })
        columns.push({
            value: dates[j]
        })
        columns.push({
            value: descriptions[j]
        })
        columns.push({
            value: deliveryModes[j]
        })
        data.push(columns)
    }
    data.push([])

    await writeXlsxFile(data, {
        filePath: 'Result.xlsx'
    })
}

scrapeWorkday()
// Readable Stream.
// readXlsxFile(fs.createReadStream('Sample.xlsx')).then((rows) => {
// // `rows` is an array of rows
// // each row being an array of cells.
//     console.log("Rows ", rows)
// })

