Web Scraper built on top of Node.js using Puppeteer for headless Chrome. 
Uses Sample.xlsx file from base/root path to look for queries and uses them as search queries on Google Patents. 
Scrapes Title, Author, Patent Code, Filing Data, Abstract from the first page of Google Patent Search for all the results and creates a file Result.xlsx on the base/root path. 
Can be configured. 
    Change the Sample file for different search queries. 
    Change the Url to scrape a different page
    Change the selectors to scrape different element


Running Dijstra! 
- Check if node is installed. If not, install it. 
- Install all the node_modules. 
- Install nodemon/node
- Run nodemon/node index.js

`brew install node`
`npm i`
`npm install nodemon`
`nodemon index.js`
