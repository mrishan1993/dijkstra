const request = require("request-promise")
const cheerio = require("cheerio")
const readXlsxFile = require('read-excel-file/node')
const writeXlsxFile = require('write-excel-file/node')
const puppeteer = require('puppeteer');
const _ = require("lodash")

const scrapURL = "https://en.wikipedia.org/wiki/Dijkstra%27s_algorithm"

var nextCounter = 0


async function sample () {
    try {
        const browser = await puppeteer.launch({

            headless: false,
            executablePath: '/Applications/Google Chrome.app/Contents/MacOS/Google Chrome',
            args: ["--app-shell-host-window-size=1600x1239"]
    
    
        });
        let queryStrings = []
        let finalResult = []
        await readXlsxFile('Sample.xlsx').then((rows) => {
            // `rows` is an array of rows
            // each row being an array of cells.
            result = []
            
            queryString = ''
            
            for (var i=0;i<rows.length;i++) {
                for (var j=0;j<rows[i].length;j++) {
                    queryString = queryString + " " +rows[i][j]
                }
                let object = {
                    queryString: queryString
                }
                queryString = queryString.replace(/ /g, "+")
                queryStrings.push(queryString)
                queryString = ''
                finalResult.push(object)
            }
        })
        var finalTitles = []
        for (var i=0;i<10;i++) { // need to change the max limit of i based on the number of pages of the transactions
            let googleURL =  "https://etherscan.io/txs?a=0x8481a6ebaf5c7dabc3f7e09e44a89531fd31f822&p=" + (i + 1)
            const page = await browser.newPage();
            let results = []
            await page.goto(googleURL, { waitUntil: 'networkidle0' });
            const titles = await page.evaluate(() => Array.from(document.querySelectorAll("a.hash-tag"), element => element.textContent));
            // const patentCodes = await page.evaluate(() => Array.from(document.querySelectorAll("h4>span.bullet-before.style-scope.search-result-item>span.style-scope.search-result-item, a>span.style-scope.search-result-item"), element => element.textContent));
            // const abstracts = await page.evaluate(() => Array.from(document.querySelectorAll("div.flex.style-scope.search-result-item>raw-html.style-scope.search-result-item>span.style-scope.raw-html"), element => element.textContent));
            // const filedDates = await page.evaluate(() => Array.from(document.querySelectorAll("h4.dates.style-scope.search-result-item"), element => element.textContent));
            // const authors = await page.evaluate(() => Array.from(document.querySelectorAll("span.bullet-before.style-scope.search-result-item>raw-html.style-scope.search-result-item>span.style-scope.raw-html"), element => element.textContent));
            // console.log(titles)
            for (let j=0;j<titles.length;j++) {
                if (titles[j].includes(".eth")) {
                    finalTitles.push(titles[j])
                }
            }
            console.log(finalTitles)

            // for (var j=0;j<titles.length;j++) {
            //     let index = filedDates[j].indexOf("Filed")
            //     let filedDate = filedDates[j].substring(index+6, index + 16)
            //     var object = {
            //         title: titles[j],
            //         patentCode: patentCodes[j],
            //         abstract: abstracts[j],
            //         filedDate: filedDate,
            //         author: authors[j*2]
            //     }
            //     results.push(object)
            // }
            // finalResult[i]["searchResults"] = results
        }
        await browser.close();
        var data = []
        for (var i =0;i<finalTitles.length;i++) {
            let sample = [
                {
                    type: String,
                    value: finalTitles[i]
                }
            ]
            data.push(sample)
        }
        // When passing `data` for each cell.
        // const data = [
        // ]
        // for (var i=0;i<finalResult.length;i++) {

        //     let query = [{
        //         "value" : finalResult[i].queryString,
        //         fontWeight: 'bold'
        //     }]
        //     data.push(query)
        //     let headers = []
        //     let title = {
        //         "value" : "Title",
        //         fontWeight: 'bold'
        //     }
        //     headers.push(title)
        //     let patentCode = {
        //         "value" : "Patent Code",
        //         fontWeight: 'bold'
        //     }
        //     headers.push(patentCode)
        //     let author = {
        //         "value" : "Author",
        //         fontWeight: 'bold'
        //     }
        //     headers.push(author)
        //     let filedDate = {
        //         "value" : "Filed Date",
        //         fontWeight: 'bold'
        //     }
        //     headers.push(filedDate)
        //     let abstract = {
        //         "value" : "Abstract",
        //         fontWeight: 'bold'
        //     }
        //     headers.push(abstract)
        //     data.push(headers)
        //     for (var j=0;j<finalResult[i].searchResults.length;j++) {
        //         let columns = []
        //         columns.push ({
        //             value: finalResult[i].searchResults[j].title
        //         })
        //         columns.push({
        //             value: finalResult[i].searchResults[j].patentCode
        //         })
        //         columns.push({
        //             value: finalResult[i].searchResults[j].author
        //         })
        //         columns.push({
        //             value: finalResult[i].searchResults[j].filedDate
        //         })
        //         columns.push({
        //             value: finalResult[i].searchResults[j].abstract
        //         })
        //         data.push(columns)
        //     }
        //     data.push([])
        // }
        await writeXlsxFile(data, {
            filePath: 'Result.xlsx'
        })
    } catch (e) {
        console.log("Exception ", e)
    }
        
}
sample()

  
// Readable Stream.
// readXlsxFile(fs.createReadStream('Sample.xlsx')).then((rows) => {
// // `rows` is an array of rows
// // each row being an array of cells.
//     console.log("Rows ", rows)
// })

